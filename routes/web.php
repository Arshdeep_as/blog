<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/
Route::get('/','PostsController@home');
//view list of posts
Route::get('/posts', 'PostsController@index');

//view form to create new posts
Route::get('/posts/create', 'PostsController@create')->middleware('admin');
Route::post('/posts', 'PostsController@store');

//view datails of a post
Route::get('/posts/{post}', 'PostsController@show');

Route::get('/archives/{year}/{month}', 'PostsController@archive');

Route::get('/posts/categories/{category}', 'CategoriesController@index');

//route to handle form post requests.
Route::post('/posts', 'PostsController@store')->middleware('admin');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/posts/comment', 'CommentsController@store');

Route::get('/posts/{post}/edit', 'PostsController@edit')->middleware('admin');
Route::patch('/posts', 'PostsController@update')->middleware('admin');

Route::get('/about', function () {
    return view('about');
});


