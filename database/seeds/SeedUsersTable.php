<?php

use Illuminate\Database\Seeder;

class SeedUsersTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //factory(\App\User::class, 50)->create();    
    	
    	DB::table('users')->insert([
    		'name' => 'Arshdeep Singh',
    		'email' => 'arsh@gmail.com',
    		'is_admin' => 1,
    		'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm' // secret
    	]);

    	DB::table('users')->insert([
    		'name' => 'Arshdeep',
    		'email' => 'arsh1@gmail.com',
    		'is_admin' => 0,
    		'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm' // secret
    	]);

    	DB::table('users')->insert([
    		'name' => 'Navdeep',
    		'email' => 'nav@gmail.com',
    		'is_admin' => 0,
    		'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm' // secret
    	]);
    }
}
