<?php

use Illuminate\Database\Seeder;

class SeedCategoriesTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
        	'name' => 'Java',
	        'created_at' => Carbon\Carbon::now(),
	        'updated_at' => Carbon\Carbon::now()
        ]);
        DB::table('categories')->insert([
        	'name' => 'C++',
	        'created_at' => Carbon\Carbon::now(),
	        'updated_at' => Carbon\Carbon::now()
        ]);
        DB::table('categories')->insert([
        	'name' => 'C',
	        'created_at' => Carbon\Carbon::now(),
	        'updated_at' => Carbon\Carbon::now()
        ]);
        DB::table('categories')->insert([
        	'name' => 'Python',
	        'created_at' => Carbon\Carbon::now(),
	        'updated_at' => Carbon\Carbon::now()
        ]);
        DB::table('categories')->insert([
        	'name' => 'C#',
	        'created_at' => Carbon\Carbon::now(),
	        'updated_at' => Carbon\Carbon::now()
        ]);
        DB::table('categories')->insert([
        	'name' => 'PHP',
	        'created_at' => Carbon\Carbon::now(),
	        'updated_at' => Carbon\Carbon::now()
        ]);
        DB::table('categories')->insert([
        	'name' => 'Ruby',
	        'created_at' => Carbon\Carbon::now(),
	        'updated_at' => Carbon\Carbon::now()
        ]);

        DB::table('category_post')->insert([
        	'category_id' => '1',
        	'post_id' => '3'
        ]);
        DB::table('category_post')->insert([
        	'category_id' => '2',
        	'post_id' => '2'
        ]);
        DB::table('category_post')->insert([
        	'category_id' => '3',
        	'post_id' => '1'
        ]);
        DB::table('category_post')->insert([
        	'category_id' => '4',
        	'post_id' => '4'
        ]);
    }
}
