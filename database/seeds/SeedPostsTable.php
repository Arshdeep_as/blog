<?php

use Illuminate\Database\Seeder;

class SeedPostsTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //factory(\App\Post::class, 50)->create();
                
        DB::table('posts')->insert([
	        'title' => 'Introduction to C',
	        'excerpt' => 'C is a procedural programming language. It was initially developed by Dennis Ritchie between 1969 and 1973. It was mainly developed as a system programming language to write operating system. ',
	        'user_id' => 1,
	        'body' =>  'The main features of C language include low-level access to memory, simple set of keywords, and clean style, these features make C language suitable for system programming like operating system or compiler development. Many later languages have borrowed syntax/features directly or indirectly from C language. Like syntax of Java, PHP, JavaScript and many other languages is mainly based on C language. C++ is nearly a superset of C language (There are few programs that may compile in C, but not in C++).',
	        'featured_image' =>'c_featured.jpg',
	        'thumbnail_image' => 'c_thumbnail.jpg',
	        'created_at' => Carbon\Carbon::now()->subDays(4),
	        'updated_at' => Carbon\Carbon::now()->subDays(4)
	    ]);

	    DB::table('posts')->insert([
	        'title' => 'Introduction to C++',
	        'excerpt' => 'C++ is a statically-typed, free-form, (usually) compiled, multi-paradigm, intermediate-level general-purpose middle-level programming language.',
	        'user_id' => 1,
	        'body' =>  'In simple terms, C++ is a sophisticated, efficient and a general-purpose programming language based on C. It was developed by Bjarne Stroustrup in 1979. Many of today’s operating systems, system drivers, browsers and games use C++ as their core language. This makes C++ one of the most popular languages today. Since it is an enhanced/extended version of C programming language, C and C++ are often denoted together as C/C++.',
	        'featured_image' =>'cpp_featured.jpg',
	        'thumbnail_image' => 'cpp_thumbnail.jpg',
	        'created_at' => Carbon\Carbon::now()->subDays(3),
	        'updated_at' => Carbon\Carbon::now()->subDays(3)
	    ]);

	    DB::table('posts')->insert([
	        'title' => 'Introduction to Java',
	        'excerpt' => 'Java is a programming language and computing platform first released by Sun Microsystems in 1995. ',
	        'user_id' => 1,
	        'body' =>  'There are lots of applications and websites that will not work unless you have Java installed, and more are created every day. Java is fast, secure, and reliable. From laptops to datacenters, game consoles to scientific supercomputers, cell phones to the Internet, Java is everywhere!',
	        'featured_image' =>'java_featured.jpg',
	        'thumbnail_image' => 'java_thumbnail.jpg',
	        'created_at' => Carbon\Carbon::now()->subDays(2),
	        'updated_at' => Carbon\Carbon::now()->subDays(2)
	    ]);

	    DB::table('posts')->insert([
	        'title' => 'Introduction to Python',
	        'excerpt' => 'Python is an interpreted high-level programming language for general-purpose programming. Created by Guido van Rossum and first released in 1991.',
	        'user_id' => 1,
	        'body' =>  'Python is a general-purpose interpreted, interactive, object-oriented, and high-level programming language. It was created by Guido van Rossum during 1985- 1990. Like Perl, Python source code is also available under the GNU General Public License (GPL). This tutorial gives enough understanding on Python programming language.',
	        'featured_image' =>'python_featured.jpg',
	        'thumbnail_image' => 'python_thumbnail.jpg',
	        'created_at' => Carbon\Carbon::now()->subDays(1),
	        'updated_at' => Carbon\Carbon::now()->subDays(1)
	    ]);
	    

	    
    }
}
