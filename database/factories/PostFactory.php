<?php

use Faker\Generator as Faker;
use Carbon\Carbon;
$factory->define(\App\Post::class, function (Faker $faker) {
    return [
        'title'=> $faker->sentence,
        'body'=> $faker->realText,
        'featured_image' => $faker->image('public/images',400,300, null, false),
        'thumbnail_image' => $faker->image('public/images/thumb',400,300, null, false),
        'created_at' => $faker->dateTimeThisYear,
        'updated_at' => Carbon::now()
    ];
});
