<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Arshdeep Blog</title>

    <!-- Bootstrap core CSS -->
  <!--
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.1.3/lux/bootstrap.min.css">
  -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"  >

    <!-- Custom styles for this template -->
    <link href="/css/blog.css" rel="stylesheet">
    <link href="/css/carousel.css" rel="stylesheet">
  </head>

  <body>

    <!-- Navigation -->
    @include('layouts.partials.nav')

    <!-- Page Content -->

    <div class="container">
      <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <!--
          <div class="carousel-item">
            @foreach($posts as $post)            
              <a href="/posts/{{$post->id}}"><img class="d-block w-100" src="/images/{{ $post->featured_image }}" alt="First slide" /></a>
            @endforeach
          </div>
          -->
          <div class="carousel-item active">
            <a href="/posts/{{$posts[0]->id}}"><img class="d-block w-100" src="/images/{{ $posts[0]->featured_image }}" alt="First slide" /></a>
          </div>
          <div class="carousel-item">
            <a href="/posts/{{$posts[1]->id}}"><img class="d-block w-100" src="/images/{{ $posts[1]->featured_image }}" alt="Second slide" /></a>
          </div>
          <div class="carousel-item">
            <a href="/posts/{{$posts[2]->id}}"><img class="d-block w-100" src="/images/{{ $posts[2]->featured_image }}" alt="Third slide" /></a>
          </div>
          <div class="carousel-item">
            <a href="/posts/{{$posts[3]->id}}"><img class="d-block w-100" src="/images/{{ $posts[3]->featured_image }}" alt="Fourth slide" /></a>
          </div>
        
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>

      <div class="row">

        <!-- Blog Entries Column -->
        @yield('content')
      
      
      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    @include('layouts.partials.footer')
    

    <!-- Bootstrap core JavaScript -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

  </body>

</html>
