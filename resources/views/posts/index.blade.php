@php($slug = "posts")

@extends('layouts.master')

@section('content')
  <div class="col-md-8">

    <h1 class="my-4">All Published Posts 
      <small>about programming</small>
    </h1>

    <!-- loop to iterate through the posts stored in the db. -->
    @php($arr = [1,2,3,4,5,6,7,8,9,10,11,12,13])

    @foreach($posts as $post)
    <!-- Blog Post -->
    @php($random_key = array_rand($arr, 1)+1)
    <div class="card mb-4">
      <img class="card-img-top" src="/images/tags/{{$random_key.'.jpg'}}" alt="Card image cap">
      <div class="card-body">
        <h2 class="card-title">{{ $post->title }}</h2>
        <p class="card-text">{{ $post->body }}</p>
        <a href="/posts/{{$post->id}}" class="btn btn-success">Read More</a>
        @if(Auth::check() && Auth::user()->is_admin)
          <a href="/posts/{{$post->id}}/edit" class="btn btn-danger">EDIT Post</a>
        @endif
      </div>
      <div class="card-footer text-muted">
        Posted on {{ $post->created_at->toFormattedDateString() }} by
        <a href="#">WDD Blogger</a>
      </div>
    </div>
    @endforeach

    <!-- Pagination -->
    @include('layouts.partials.paginate')

  </div>

@endsection