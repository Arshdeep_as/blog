@php($slug = "posts")

@extends('layouts.master') 

@section('content')
	<div class="col-md-8">
	    <h1 class="my-4">
	      <small>Posts...</small>
	    </h1>
	    @foreach($posts as $post)
		    <div class="card mb-4">
		      <img class="card-img-top" src="/images/tags/{{$category->name.'.jpg'}}" alt="Card image cap">
		      <div class="card-body">
		        <h2 class="card-title">{{ $post->title }}</h2>
		        <p class="card-text">{{ $post->body }}</p>
		        <a href="/posts/{{$post->id}}" class="btn btn-success">Read More &rarr;</a>
		      </div>
		      <div class="card-footer text-muted">
		        Posted on {{ $post->created_at->toFormattedDateString() }} by
		        <a href="#">WDD Blogger</a>
		      </div>
		    </div>
	    @endforeach
	    
	    
	</div>
@endsection