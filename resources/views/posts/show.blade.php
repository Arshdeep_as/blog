@php($slug = "posts")

@extends('layouts.master')

@section('content')
	<div class="col-md-8">
		<!-- Title -->
        <h1 class="mt-4">{{ $post->title }}</h1>

        @if(count($post->categories))
        	<ul>
        		@foreach($post->categories as $category)
        			<li> <a style="color: #fff" href="/posts/categories/{{ $category->name }}">{{ $category->name }}</a> </li>
        		@endforeach
        	</ul>
        @endif
		<!-- Author -->
		<p style="color: #fff" class="lead">
		by
		<a style="color: #fff" href="#">WDD Blogger</a>
		</p>

		<hr style="color: #fff">

		<!-- Date/Time -->
		<p style="color: #fff">Posted on January 1, 2018 at 12:00 PM</p>

		<hr style="color: #fff">
		@if(Auth::check() && Auth::user()->is_admin)
			<a style="color: #fff" href="/posts/{{$post->id}}/edit" class="btn btn-danger">EDIT Post&rarr;</a>
		@endif
		<hr>

		<!-- Preview Image -->
		<img class="img-fluid rounded" src="/images/tags/{{ $category->name.'.jpg' }}" alt="">

		<hr style="color: #fff">

		<!-- Post Content -->
		<p style="color: #fff" class="lead">{{ $post->excerpt }}</p>

		<p style="color: #fff">{{ $post->body }}</p>

		<p style="color: #fff">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>

		<p style="color: #fff">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>

		<hr style="color: #fff">
		<!-- Checking if the user is logged in or not, if its not logged in then he will not be able to sebmit comments. -->

		@if (Auth::check())
     	<!-- Comments Form -->
		<div class="card my-4">
			<h5 class="card-header">Leave a Comment:</h5>
			<div class="card-body">
			    <!-- /posts/ id, post with that id, comments fields to store commets for that post -->
			  <form  method="post" action="/posts/comment">
			  	<!-- including errors blade to display the errors for comments -->
				@include('layouts.partials.errors')
				{{ csrf_field() }}
				<input type="hidden" name="post_id" value="{{$post->id}}">
			    <div class="form-group">
			      <textarea class="form-control" name="body" rows="3"></textarea>
			    </div>
			    <button type="submit" class="btn btn-primary">Submit</button>
			  </form>
			</div>
		</div>
		<!-- if user is not logged in, then displaying the message that says user needs to be logged in to make comments -->
		@else
			<div class="alert alert-info">
				<p>You must be logged in to leave a comment...</p>
			</div>
		@endif
			<!-- Displaying each comment coressponding to that post -->
			@foreach($post->comments()->get() as $comment)
			<!-- Single Comment -->
			<div class="media mb-4">
				<img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
				<div class="media-body">
				  <h5 style="color: #fff" class="mt-0">WDD Blogger</h5>
				  <p style="color: #fff">{{ $comment->body }}</p>
				</div>
			</div>
			@endforeach
	    </div>
@endsection