@php($slug = "create")

@extends('layouts.master')

@section('content')
  <div class="col-md-8">
    <h1 class="my-4">Create new POST
      <small>Easily..</small>
    </h1>

    @if (Auth::check())
        <h2>You are logged IN!!!</h2>
    @endif

    <form id="my_form" method="post" action="/posts">

        @include('layouts.partials.errors')

        {{ csrf_field() }}
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" id="title" name="title" value="{{ old('title') }}" placeholder="Enter post title">
        </div>

        <div class="form-group">
            <label for="excerpt">Excerpt</label>
            <input type="text" class="form-control" id="excerpt" name="excerpt" value="{{ old('excerpt') }}" placeholder="Enter post excerpt">
        </div>

        <div class="form-group">
            <label for="body">Body</label>
            <textarea class="form-control" id="body" name="body" placeholder="Enter post body">{{ old('body') }}</textarea>
        </div>

        <div class="form-group">
            <label for="featured_image">Featured Image</label>
            <input type="text" class="form-control" id="featured_image" name="featured_image" value="{{ old('featured_image') }}" placeholder="Enter image name eg image_name.jpg">
        </div>

        <div class="form-group">
            <label for="thumbnail_image">Thumbnail Image</label>
            <input type="text" class="form-control" id="thumbnail_image" name="thumbnail_image" value="{{ old('thumbnail_image') }}" placeholder="Enter image name eg image_name.jpg">
        </div>

    <!--
      <div class="form-group">
        <label for="author">Author Name</label>
        <input type="text" class="form-control" id="author" name="author" placeholder="Enter post author">
      </div>
    -->  

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
    
  </div>

@endsection