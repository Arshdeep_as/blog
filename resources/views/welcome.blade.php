@php($slug = "home")

@extends('layouts.master_home')

<h1>“Everybody should learn to program a computer, because it teaches you how to think.”<small>-Steve Jobs</small></h1>

@section('content')

  <h3>To read more <strong>Comments</strong>, please go to <a href="/posts">Posts Page</a>, and refresh it.</h3>
  <!-- Page Features -->
  <div class="row text-center">

  	@foreach($posts as $post)
    <div class="col-lg-3 col-md-6 mb-4">
      <div class="card">
        <img class="card-img-top" src="/images/thumb/{{ $post->thumbnail_image }}" alt="">
        <div id="card_body" class="card-body">
          <h4 class="card-title">{{$post->title}}</h4>
          <p class="card-text">{{$post->excerpt}}</p>
        </div>
        <div id="card_footer" class="card-footer">
          <a href="/posts/{{$post->id}}" id="my_button" class="btn btn-success">Whole Post</a>
        </div>
      </div>
    </div>
    @endforeach
    <!--
    <div class="col-lg-3 col-md-6 mb-4">
      <div class="card">
        <img class="card-img-top" src="http://placehold.it/500x325" alt="">
        <div class="card-body">
          <h4 class="card-title">Card title</h4>
          <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo magni sapiente, tempore debitis beatae culpa natus architecto.</p>
        </div>
        <div class="card-footer">
          <a href="#" class="btn btn-primary">Find Out More!</a>
        </div>
      </div>
    </div>

    <div class="col-lg-3 col-md-6 mb-4">
      <div class="card">
        <img class="card-img-top" src="http://placehold.it/500x325" alt="">
        <div class="card-body">
          <h4 class="card-title">Card title</h4>
          <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente esse necessitatibus neque.</p>
        </div>
        <div class="card-footer">
          <a href="#" class="btn btn-primary">Find Out More!</a>
        </div>
      </div>
    </div>

    <div class="col-lg-3 col-md-6 mb-4">
      <div class="card">
        <img class="card-img-top" src="http://placehold.it/500x325" alt="">
        <div class="card-body">
          <h4 class="card-title">Card title</h4>
          <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo magni sapiente, tempore debitis beatae culpa natus architecto.</p>
        </div>
        <div class="card-footer">
          <a href="#" class="btn btn-primary">Find Out More!</a>
        </div>
      </div>
    </div>
	-->
  </div>
  <!-- /.row -->
@endsection