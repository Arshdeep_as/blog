@php($slug = "about")

@extends('layouts.master')

@section('content')

<div class="col-md-8">
<h1>Arshdeep Singh</h1>
Programmer & Web Developer from Winnipeg

<p>I am an emerging web developer with one year experience in developing websites for university projects as a main developer. After completing web development diploma and an internship with xyz company, I asked myself what next? now I feel to move one step forward and start my career as a professional web developer. I am passionate about coding as I am very much indulge towards it from last 8 years.
</p>
<p>
Logical thinking, as I worked on many word press websites and I find it very interesting to find and solve any logical flaws while developing them. Creative, love to develop new website designs and user interaction in a creative way, which looks attractive and easy to use.
</p>
<p>
Team player, I really enjoyed working with other people on projects as it brings many new possibilities of solving the same problem.
</p><p>
While studying web development diploma in university of Winnipeg, I have to make a WordPress ecommerce website according to a predefined requirements. Some of the requirements are not as easy as they seem, for example creating a large UI design for mobile devices.
</p>
</div>
@endsection