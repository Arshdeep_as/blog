<?php

namespace App;
use App\Comment;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Auth;
class Post extends Model
{
    protected $fillable = ['title', 'body','user_id', 'excerpt', 'featured_image', 'thumbnail_image'];
    //protected $guarded = [];
    
    //$post->comment();
    
    public function comments()
    {
    	return $this->hasMany('App\Comment');
    }

    
    public function scopeFilter($query, $filter)
    {
    	/*
    	if($month = $filter['month']){
            $query->whereMonth('created_at', Carbon::parse($month)->month);
        }
        if($year = $filter['year']){
            $query->whereYear('created_at', $year);
        }
		*/
        //$posts = $posts->get();
        
        $month = Carbon::parse($filter['month'])->month;

        return Post::latest()
        			->whereYear('created_at', $filter['year'])
        			->whereMonth('created_at', $month);
    }
    
   
   public static function archives()
   {
   		return Post::selectRaw('year(created_at) year, monthname(created_at) month, count(*) published')
        ->groupBy('year', 'month')
        ->orderByRaw('min(created_at) desc')
        ->get()
        ->toArray();
   }

   public function categories()
   {
        return $this->belongsToMany(Category::class);
   }
   
}
