<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Post;
class Comment extends Model
{
    //making a list of fillables, fields that are allowed to be filled in the table
    protected $fillable = ['user_id', 'post_id', 'body'];

    /**
     * method to return the relation between post and comment
     * @return relation
     */
    public function post()
    {
    	return $this->belongTo('App\Post');
    }

    /**
     * method to return the relation between user and comment
     * @return relation
     */
    public function user()
    {
    	return $this->belongTo('App\User')->first();
    }
}

