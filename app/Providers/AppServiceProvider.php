<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        //provides archives to the sidebar.
        /*
        view()->composer('layouts.partials.sidebar', function($view){
            $view->with('archives', \App\Post::archives());
            $view->with('categories', \App\Category::has('posts')->pluck('name'));
        });
        */
        //cleaner version of above code is
        view()->composer('layouts.partials.sidebar', function($view){
            $archives = \App\Post::archives();
            $categories = \App\Category::has('posts')->pluck('name');
            $view->with(compact('archives', 'categories'));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
