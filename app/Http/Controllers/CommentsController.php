<?php

namespace App\Http\Controllers;
use App\Comment;
use App\Post;
use Illuminate\Http\Request;
use Auth;

class CommentsController extends Controller
{
	/**
	 * method to store comment to the database, first validate it and then storeit.
	 * @param  Request $request storing the request object
	 * @return [redirect]
	 */
    public function store(Request $request)
    {
    	//validating the comments form data
    	$request->validate([
            'body' => 'required'
       ]);

    	//inserting data to the comments table.
    	Comment::create([
    		'user_id' =>  Auth::id(),
    		'post_id' => request('post_id'),
    		'body' => request('body')
    	]);
    	//returning a redirect to the posts page from where the request has been made.
    	return redirect()->back();
    }
}
