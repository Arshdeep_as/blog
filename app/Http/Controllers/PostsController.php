<?php

namespace App\Http\Controllers;

use App\Post;
use Carbon\Carbon;
use Auth;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return session('message');
        $posts = Post::latest()->paginate(5);
        /*
        $posts = Post::latest()->filter(request(['month', 'year']))->get();
        $archives = Post::selectRaw('year(created_at) year, monthname(created_at) month, count(*) published')
        ->groupBy('year', 'month')
        ->orderByRaw('min(created_at) desc')
        ->get()
        ->toArray();
        */

        return view('posts.index', compact('posts'));
    }
    /**
     * Show the home page.
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        $posts = Post::latest()->limit(4)->get();
        return view('welcome', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //dd(request()-> all());
        /* one way of doing this is:
        $post = new Post;
        $post->title = request('title');
        $post->title = request('body');
        $post->save();

        return redirect('/posts');
        */
       
       /* if you are using $request object, validator will be works as this.
       store(Request $request)
       $request->validate([
            'title' => 'required|min:2|max:255',
            'body' => 'required|min:20'
       ]);
       */
        $this->validate(request(),[
            'title' => 'required|min:2|max:255',
            'body' => 'required|min:20',
            'excerpt' => 'required|min:2|max:255',
            'featured_image' => 'required|min:5|max:255',
            'thumbnail_image' => 'required|min:5|max:255'
        ]);

       //prefferd way of doing this is:
        Post::create([
            'title' => request('title'),
            'body' => request('body'),
            'excerpt' => request('excerpt'),
            'user_id' => Auth::id(),
            'featured_image' => request('featured_image'),
            'thumbnail_image' => request('thumbnail_image')
        ]);

        //to use session control we can use it in to ways, either by calling session method with reques tmethod or with directly using laravel session method.
        // request()->session() or 
        session()->flash('message', 'Post Created Successfully');
        return redirect('/posts');
    }
    /**
     * shows the archives sidebar
     * @param  String $year  
     * @param  String $month 
     * @return \Illuminate\Http\Response
     */
    public function archive($year, $month)
    {
        $posts = Post::filter(['year' => $year, 'month' => $month])->paginate(5);
        return view('posts.archive', compact('posts','year','month'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        $this->validate(request(),[
            'title' => 'required|min:2|max:255',
            'body' => 'required|min:20',
            'excerpt' => 'required|min:2|max:255',
            'featured_image' => 'required|min:5|max:255',
            'thumbnail_image' => 'required|min:5|max:255',
            'post_id'=>'required'
        ]);
        //dd(request(['post_id','title','body']));
        $post = Post::find(request('post_id'));
        $post->title = request('title');
        $post->excerpt = request('excerpt');
        $post->body = request('body');
        $post->featured_image = request('featured_image');
        $post->thumbnail_image = request('thumbnail_image');
        $post->save();

        return redirect("/posts/".request('post_id'));
        //or we can do this as =>  return redirect("/posts/".$post->post_id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
