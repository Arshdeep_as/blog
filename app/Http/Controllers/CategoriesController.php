<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use App\Category;
use Carbon\Carbon;

class CategoriesController extends Controller
{
	/**
	 * gives the categories of the sidebar
	 * @param  Category|null $category 
	 * @return Illuminate\Http\Response               
	 */
    public function index(Category $category = null)
    {
    	$posts = $category->posts;
    	
    	return view('posts.category', compact('posts', 'category'));
    }
}
